package com.example.firstdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class ProcessCreditCardApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProcessCreditCardApplication.class, args);
	}
}
