package com.example.firstdata.response.dto;

import java.math.BigDecimal;

public class ResponseCheckRate extends BaseResponse {
	
	private BigDecimal rate;

	public BigDecimal getRate() {
		return rate;
	}

	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}

}
