package com.example.firstdata.response.dto;

import com.example.firstdata.domain.PaymentMethodHolder;

public class ResponseCreditCard extends BaseResponse{
	
	private String surname;
	private String bank;
	private String cardNumberMask;
	private String expirationDate;
	private Integer pmHolderStatus;

	public ResponseCreditCard(PaymentMethodHolder pmh) {
		this.setSurname(pmh.getCardHolder().getSurname());
		this.setBank(pmh.getPaymentMethodBank().getBank().getName());
		this.setCardNumberMask(pmh.getCardNumberMask());
		this.setExpirationDate(pmh.getExpirationDate());
		this.setPmHolderStatus(pmh.getPmHolderStatus());
	}

	public ResponseCreditCard() {
		// TODO Auto-generated constructor stub
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getCardNumberMask() {
		return cardNumberMask;
	}

	public void setCardNumberMask(String cardNumberMask) {
		this.cardNumberMask = cardNumberMask;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Integer getPmHolderStatus() {
		return pmHolderStatus;
	}

	public void setPmHolderStatus(Integer pmHolderStatus) {
		this.pmHolderStatus = pmHolderStatus;
	}

}
