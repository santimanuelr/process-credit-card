package com.example.firstdata.services;

import com.example.firstdata.response.dto.ResponseCreditCard;

public interface CreditCardServ {

	public ResponseCreditCard getCreditCardInformation(String numberCreditCard);
	
	public boolean validCreditCard();
	
	public boolean equalCreditCard();
	
}
