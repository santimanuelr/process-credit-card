package com.example.firstdata.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.firstdata.domain.PaymentMethodHolder;
import com.example.firstdata.repository.PaymentMethodHolderRepo;
import com.example.firstdata.response.dto.ResponseCreditCard;
import com.example.firstdata.services.CreditCardServ;

@Service
public class CreditCardServImpl implements CreditCardServ {	
	
	@Autowired PaymentMethodHolderRepo paymentMethodHolderRepo;

	@Override
	public boolean validCreditCard() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean equalCreditCard() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ResponseCreditCard getCreditCardInformation(String numberCreditCard) {
		PaymentMethodHolder pmh = paymentMethodHolderRepo.findByCardNumberMask(numberCreditCard);
		return new ResponseCreditCard(pmh);
	}	
	
}
