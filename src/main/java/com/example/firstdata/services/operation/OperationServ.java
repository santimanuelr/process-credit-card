package com.example.firstdata.services.operation;

import com.example.firstdata.response.dto.ResponseCheckRate;

public interface OperationServ {

	public boolean validOperation();
	
	public ResponseCheckRate getRateByBrandAndImport(String creditCardMark, String expirationDate);
	
}
