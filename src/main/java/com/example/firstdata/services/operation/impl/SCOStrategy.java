package com.example.firstdata.services.operation.impl;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.example.firstdata.services.operation.OpCardRateStrgyServ;
import com.example.firstdata.utils.UtilDate;

@Service
public class SCOStrategy implements OpCardRateStrgyServ {

	private static final double _SCO_VALUE = 0.5;

	@Override
	public BigDecimal getRateByBrandAndImport(String creditCardMark, String expirationDate) {
		return new BigDecimal(UtilDate.getMonthDay() * _SCO_VALUE);
	}

}
