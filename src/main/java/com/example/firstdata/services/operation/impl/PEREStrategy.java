package com.example.firstdata.services.operation.impl;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.example.firstdata.services.operation.OpCardRateStrgyServ;
import com.example.firstdata.utils.UtilDate;

@Service
public class PEREStrategy implements OpCardRateStrgyServ {

	private static final double _PERE_VALUE = 0.1;

	@Override
	public BigDecimal getRateByBrandAndImport(String creditCardMark, String expirationDate) {
		return new BigDecimal(UtilDate.getMonth() * _PERE_VALUE);
	}

}
