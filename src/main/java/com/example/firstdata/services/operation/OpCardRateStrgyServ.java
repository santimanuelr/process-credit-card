package com.example.firstdata.services.operation;

import java.math.BigDecimal;

public interface OpCardRateStrgyServ {
	
	public BigDecimal getRateByBrandAndImport(String creditCardMark, String expirationDate);

}
