package com.example.firstdata.services.operation.impl;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.firstdata.response.dto.ResponseCheckRate;
import com.example.firstdata.services.operation.OperationServ;

@Service
public class OperationServImpl implements OperationServ {

	@Autowired private PEREStrategy pereStrategy;
	@Autowired private SCOStrategy scoStrategy;
	@Autowired private SQUAStrategy squaStrategy;

	@Override
	public boolean validOperation() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ResponseCheckRate getRateByBrandAndImport(String creditCardMark, String expirationDate) {
		ResponseCheckRate response = new ResponseCheckRate();
		BigDecimal result = new BigDecimal(0);
		switch (creditCardMark) {
		case "PERE":
			result = pereStrategy.getRateByBrandAndImport(creditCardMark, expirationDate);
			break;
		case "SCO":
			result = scoStrategy.getRateByBrandAndImport(creditCardMark, expirationDate);
			break;
		case "SQUA":
			result = squaStrategy.getRateByBrandAndImport(creditCardMark, expirationDate);
			break;
		default:
			break;
		}
		response.setMessage("OK");
		response.setRate(result);
		return response;
	}

}
