package com.example.firstdata.services.operation.impl;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.example.firstdata.services.operation.OpCardRateStrgyServ;
import com.example.firstdata.utils.UtilDate;

@Service
public class SQUAStrategy implements OpCardRateStrgyServ {

	@Override
	public BigDecimal getRateByBrandAndImport(String creditCardMark, String expirationDate) {
		// TODO Auto-generated method stub
		Integer result = UtilDate.getYear(expirationDate) / UtilDate.getMonth(expirationDate);
		return BigDecimal.valueOf(result);
	}

}