package com.example.firstdata.utils;

import java.time.LocalDate;

public class UtilDate {

	public static int getYear(String expirationDate) {
		String year = expirationDate.substring(2, 5);
		return Integer.valueOf(year);
	}

	public static int getMonth(String expirationDate) {
		String year = expirationDate.substring(0, 2);
		return Integer.valueOf(year);
	}

	public static int getMonthDay() {
		LocalDate currentDate = LocalDate.now();
		return currentDate.getDayOfMonth();
	}

	public static double getMonth() {
		LocalDate currentDate = LocalDate.now();
		return currentDate.getMonthValue();
	}

}
