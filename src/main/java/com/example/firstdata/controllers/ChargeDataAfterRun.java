package com.example.firstdata.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.example.firstdata.domain.Bank;
import com.example.firstdata.domain.PaymentMethodType;
import com.example.firstdata.repository.BankRepo;
import com.example.firstdata.repository.PaymentMethodBankRepo;
import com.example.firstdata.repository.PaymentMethodHolderRepo;
import com.example.firstdata.repository.PaymentMethodRepo;
import com.example.firstdata.repository.PaymentMethodTypeRepo;

@Component
public class ChargeDataAfterRun {

	private static final Logger logger = LoggerFactory.getLogger(OperationController.class);
    @Autowired private PaymentMethodTypeRepo repoPaymentsType;
    @Autowired private BankRepo repoBanks;
    @Autowired private PaymentMethodRepo paymentMethodRepo;
    @Autowired private PaymentMethodBankRepo paymentMethodBankRepo;
    @Autowired private PaymentMethodHolderRepo paymentMethodHolderRepo;

    @EventListener
    public void appReady(ApplicationReadyEvent event) {

        /*repo.save();*/
    	/*PaymentMethodType pmt = new PaymentMethodType("Credito", "CR");
    	PaymentMethodType pmt2 = new PaymentMethodType("Devito", "DV");
    	PaymentMethodType pmt3 = new PaymentMethodType("Efectivo", "EF");
    	
    	try {
    		repoPaymentsType.save(pmt);
    		repoPaymentsType.save(pmt2);
    		repoPaymentsType.save(pmt3);			
		} catch (Exception e) {
			logger.error("Error insert types of payment: {}", e.getMessage());
		}
    	
    	Bank bank1 = new Bank("Galicia", "GAL");
    	Bank bank2 = new Bank("Ciudad", "CI");
    	
    	try {
    		repoBanks.save(bank1);
    		repoBanks.save(bank2);			
		} catch (Exception e) {
			logger.error("Error insert types of banks: {}", e.getMessage());
		}*/
    	
    	
    }

	public PaymentMethodTypeRepo getRepo() {
		return repoPaymentsType;
	}

	public void setRepo(PaymentMethodTypeRepo repo) {
		this.repoPaymentsType = repo;
	}

	public PaymentMethodTypeRepo getRepoPaymentsType() {
		return repoPaymentsType;
	}

	public void setRepoPaymentsType(PaymentMethodTypeRepo repoPaymentsType) {
		this.repoPaymentsType = repoPaymentsType;
	}

	public BankRepo getRepoBanks() {
		return repoBanks;
	}

	public void setRepoBanks(BankRepo repoBanks) {
		this.repoBanks = repoBanks;
	}

	public PaymentMethodRepo getPaymentMethodRepo() {
		return paymentMethodRepo;
	}

	public void setPaymentMethodRepo(PaymentMethodRepo paymentMethodRepo) {
		this.paymentMethodRepo = paymentMethodRepo;
	}

	public PaymentMethodBankRepo getPaymentMethodBankRepo() {
		return paymentMethodBankRepo;
	}

	public void setPaymentMethodBankRepo(PaymentMethodBankRepo paymentMethodBankRepo) {
		this.paymentMethodBankRepo = paymentMethodBankRepo;
	}

	public PaymentMethodHolderRepo getPaymentMethodHolderRepo() {
		return paymentMethodHolderRepo;
	}

	public void setPaymentMethodHolderRepo(PaymentMethodHolderRepo paymentMethodHolderRepo) {
		this.paymentMethodHolderRepo = paymentMethodHolderRepo;
	}
    
}    
