package com.example.firstdata.controllers;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.firstdata.request.dto.OperationRequest;
import com.example.firstdata.response.dto.ResponseCheckRate;
import com.example.firstdata.response.dto.ResponseCreditCard;
import com.example.firstdata.services.CreditCardServ;

@RestController
@RequestMapping("/creditCard")
public class CreditCardController {
	
	private static final Logger logger = LoggerFactory.getLogger(CreditCardController.class);
	@Autowired private CreditCardServ creditCardServ;

	public CreditCardServ getCreditCardServ() {
		return creditCardServ;
	}

	public void setCreditCardServ(CreditCardServ creditCardServ) {
		this.creditCardServ = creditCardServ;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseCreditCard> checkRate(@Valid @RequestParam String numberCreditCard) {
		logger.info("GET /opertaion");
		ResponseCreditCard response = new ResponseCreditCard();
		try {
			response = creditCardServ.getCreditCardInformation(numberCreditCard);
		} catch (Exception e) {
			logger.error("Error checkRate Method");
			response.setMessage("ERROR");
			return new ResponseEntity<ResponseCreditCard>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<ResponseCreditCard>(response, HttpStatus.OK);
	}

}
