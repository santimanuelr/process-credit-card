package com.example.firstdata.controllers;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.firstdata.request.dto.OperationRequest;
import com.example.firstdata.response.dto.ResponseCheckRate;
import com.example.firstdata.services.operation.OpCardRateStrgyServ;
import com.example.firstdata.services.operation.OperationServ;

@SuppressWarnings("unused")
@RestController
@RequestMapping("/opertaion")
public class OperationController {

	private static final Logger logger = LoggerFactory.getLogger(OperationController.class);
	@Autowired
	private OperationServ operationServ;

	@RequestMapping(value = "/checkRate", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<ResponseCheckRate> checkRate(@Valid @RequestBody OperationRequest request) {
		logger.info("GET /opertaion");
		ResponseCheckRate response = new ResponseCheckRate();
		try {
			response = operationServ.getRateByBrandAndImport(request.getCreditCardMark(), request.getExpirationDate());
		} catch (Exception e) {
			logger.error("Error checkRate Method");
			response.setMessage("ERROR");
			return new ResponseEntity<ResponseCheckRate>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<ResponseCheckRate>(response, HttpStatus.OK);
	}
	
}
