package com.example.firstdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.firstdata.domain.Operation;

public interface OperationRepo extends JpaRepository<Operation, Long> {

}
