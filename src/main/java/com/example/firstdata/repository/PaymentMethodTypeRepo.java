package com.example.firstdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.firstdata.domain.PaymentMethodType;

@Repository
public interface PaymentMethodTypeRepo extends JpaRepository<PaymentMethodType, Long> {

}
