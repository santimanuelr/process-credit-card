package com.example.firstdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.firstdata.domain.Bank;

public interface BankRepo extends JpaRepository<Bank, Long> {

}
