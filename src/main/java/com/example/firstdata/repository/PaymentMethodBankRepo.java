package com.example.firstdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.firstdata.domain.PaymentMethodBank;

public interface PaymentMethodBankRepo extends JpaRepository<PaymentMethodBank, Long> {

}
