package com.example.firstdata.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.firstdata.domain.PaymentMethodHolder;

@Repository
@Transactional
public interface PaymentMethodHolderRepo extends JpaRepository<PaymentMethodHolder, Long> {
	
	public PaymentMethodHolder findByCardNumberMask(String cardNumberMask);

}
