package com.example.firstdata.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.firstdata.domain.PaymentMethod;

@Repository
@Transactional
public interface PaymentMethodRepo extends JpaRepository<PaymentMethod, Long> {

}
