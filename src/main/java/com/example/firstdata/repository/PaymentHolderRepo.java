package com.example.firstdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.firstdata.domain.PaymentHolder;

public interface PaymentHolderRepo extends JpaRepository<PaymentHolder, Long> {

}
