package com.example.firstdata.request.dto;

import java.math.BigDecimal;

public class OperationRequest {
	
	private String creditCardMark;
	private String expirationDate;
	private BigDecimal amount;
	
	
	public OperationRequest() {
		super();
	}
	
	
	public String getCreditCardMark() {
		return creditCardMark;
	}
	public void setCreditCardMark(String creditCardMark) {
		this.creditCardMark = creditCardMark;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public String getExpirationDate() {
		return expirationDate;
	}


	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

}
