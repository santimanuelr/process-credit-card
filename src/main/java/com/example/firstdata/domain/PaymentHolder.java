package com.example.firstdata.domain;

import java.sql.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Cacheable
@Table(name = "PAYMENT_HOLDER")
public class PaymentHolder extends AuditableObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String surname;
	private String identification;
	private Date brithdayDate;
	
	public PaymentHolder() {
		super();
	}

	@Id
	@GeneratedValue
	@Column(name = "id_payment_holder")
	public Long getId() {
		return id;
	}
	
	@Column(name = "name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "surname")
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	@Column(name = "identification")
	public String getIdentification() {
		return identification;
	}
	public void setIdentification(String identification) {
		this.identification = identification;
	}	
	
	public Date getBrithdayDate() {
		return brithdayDate;
	}
	public void setBrithdayDate(Date brithdayDate) {
		this.brithdayDate = brithdayDate;
	}
	
}
