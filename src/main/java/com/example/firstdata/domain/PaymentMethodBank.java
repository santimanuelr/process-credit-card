package com.example.firstdata.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PAYMENT_METHOD_BANK")
public class PaymentMethodBank extends AuditableObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Bank bank;
	private PaymentMethod paymentMethod;
	
	
	
	
	public PaymentMethodBank() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_payment_method_bank")
	public Long getId() {
		return id;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_bank")
	public Bank getBank() {
		return bank;
	}
	public void setBank(Bank bank) {
		this.bank = bank;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_payment_method")
	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
}
