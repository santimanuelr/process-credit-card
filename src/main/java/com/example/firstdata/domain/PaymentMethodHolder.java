package com.example.firstdata.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "PAYMENT_METHOD_HOLDER")
public class PaymentMethodHolder extends AuditableObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PaymentHolder cardHolder;
	private PaymentMethodBank paymentMethodBank;
	private String cardNumberMask;
	private String expirationDate;
	private Integer pmHolderStatus;
	
	
	
	
	public PaymentMethodHolder() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_payment_method_holder")
	public Long getId() {
		return id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_payment_holder")
	public PaymentHolder getCardHolder() {
		return cardHolder;
	}
	public void setCardHolder(PaymentHolder cardHolder) {
		this.cardHolder = cardHolder;
	}
	
	@Column(name = "card_number_mask")
	@Size(min = 13)
	@NotNull
	public String getCardNumberMask() {
		return cardNumberMask;
	}
	public void setCardNumberMask(String cardNumberMask) {
		this.cardNumberMask = cardNumberMask;
	}
	
	@Column(name = "expiration_date")
	@NotNull
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	@Column(name = "pm_holder_status")
	public Integer getPmHolderStatus() {
		return pmHolderStatus;
	}
	public void setPmHolderStatus(Integer pmHolderStatus) {
		this.pmHolderStatus = pmHolderStatus;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_payment_method_bank")
	public PaymentMethodBank getPaymentMethodBank() {
		return paymentMethodBank;
	}

	public void setPaymentMethodBank(PaymentMethodBank paymentMethodBank) {
		this.paymentMethodBank = paymentMethodBank;
	}
	
}
