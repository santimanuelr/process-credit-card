package com.example.firstdata.domain;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Cacheable
@Table(name = "BANK")
public class Bank extends AuditableObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String code;
	private String name;
	
	
	public Bank(String string, String string2) {
		this.code = string;
		this.name = string2;
	}

	public Bank() {
		super();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_bank")
	public Long getId() {
		return id;
	}
	
	@Column(name = "name")
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	@Column(name = "code")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
	
}
