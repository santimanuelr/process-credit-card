package com.example.firstdata.domain;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

@MappedSuperclass
public class AuditableObject implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected Long id;
	private String userRegister;
	private Date dateRegister;
	private String userModifier;
	private Date dateModifier;
	private Long version;
	
	
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "user_register")
	public String getUserRegister() {
		return userRegister;
	}
	public void setUserRegister(String userRegister) {
		this.userRegister = userRegister;
	}
	
	@Column(name = "date_register")
	public Date getDateRegister() {
		return dateRegister;
	}
	public void setDateRegister(Date dateRegister) {
		this.dateRegister = dateRegister;
	}
	
	@Column(name = "user_modifier")
	public String getUserModifier() {
		return userModifier;
	}
	public void setUserModifier(String userModifier) {
		this.userModifier = userModifier;
	}
	
	@Column(name = "date_modifier")
	public Date getDateModifier() {
		return dateModifier;
	}
	public void setDateModifier(Date dateModifier) {
		this.dateModifier = dateModifier;
	}
	
	@Version
	@Column(name = "version")
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}

}
