package com.example.firstdata.domain;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Cacheable
@Table(name = "PAYMENT_METHOD")
public class PaymentMethod extends AuditableObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String code;
	private String name;
	private PaymentMethodType paymentMethodType;
	
	

	public PaymentMethod(String code, String name, PaymentMethodType paymentMethodType) {
		super();
		this.code = code;
		this.name = name;
		this.paymentMethodType = paymentMethodType;
	}
	
	

	public PaymentMethod() {
		super();
	}



	@Id
	@GeneratedValue
	@Column(name = "id_payment_method")
	public Long getId() {
		return id;
	}
	
	@Column(name = "name")
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	@Column(name = "code")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_payment_method_type")
	public PaymentMethodType getPaymentMethodType() {
		return paymentMethodType;
	}

	public void setPaymentMethodType(PaymentMethodType paymentMethodType) {
		this.paymentMethodType = paymentMethodType;
	}

}
