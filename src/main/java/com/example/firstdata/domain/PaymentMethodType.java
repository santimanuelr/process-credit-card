package com.example.firstdata.domain;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Cacheable
@Table(name = "PAYMENT_METHOD_TYPE")
public class PaymentMethodType extends AuditableObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String code;
	
	
	
	public PaymentMethodType(String name, String code) {
		super();
		this.name = name;
		this.code = code;
	}

	public PaymentMethodType() {
		super();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_payment_method_type")
	public Long getId() {
		return id;
	}
	
	@Column(name = "name")
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	@Column(name = "code")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
