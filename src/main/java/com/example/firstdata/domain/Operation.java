package com.example.firstdata.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "OPERATION")
public class Operation implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private BigDecimal amount;
	private PaymentHolder paymentHolder;
	private PaymentMethodBank paymentMethodBank;
	private Date date;
	private Integer resultCode;
	private BigDecimal rate;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_payment_method_holder")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "amount")
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_payment_holder")
	public PaymentHolder getPaymentHolder() {
		return paymentHolder;
	}
	public void setPaymentHolder(PaymentHolder paymentHolder) {
		this.paymentHolder = paymentHolder;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_payment_method_bank")
	public PaymentMethodBank getPaymentMethodBank() {
		return paymentMethodBank;
	}
	public void setPaymentMethodBank(PaymentMethodBank paymentMethodBank) {
		this.paymentMethodBank = paymentMethodBank;
	}
	
	@Column(name = "date_create")
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	@Column(name = "result_code")
	public Integer getResultCode() {
		return resultCode;
	}
	public void setResultCode(Integer resultCode) {
		this.resultCode = resultCode;
	}
	
	@Column(name = "rate")
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	
}
