package com.example.firtsdata;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.firstdata.ProcessCreditCardApplication;
import com.example.firstdata.domain.PaymentMethodType;
import com.example.firstdata.repository.PaymentMethodTypeRepo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProcessCreditCardApplication.class)
@TestPropertySource(locations = "classpath:application.properties")
public class ProcessCreditCardApplicationTests {

	@Autowired private PaymentMethodTypeRepo paymentMethodTypeRepo;
	
	@Test
	public void paymentMethodTypeRepo_Test() {		
		PaymentMethodType pmt = paymentMethodTypeRepo.save(new PaymentMethodType("Credito", "CR"));
		PaymentMethodType pmtFound = paymentMethodTypeRepo.getOne(pmt.getId());
		
		Assert.assertNotNull(pmtFound);
		Assert.assertEquals(pmt.getName(), pmtFound.getName());
	}

}
