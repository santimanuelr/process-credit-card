INSERT INTO `payment_method` (`id_payment_method`, `date_modifier`, `date_register`, `user_modifier`, `user_register`, `version`, `name`, `code`, `id_payment_method_type`) VALUES
	(1, NULL, NULL, NULL, NULL, NULL, 'VISA CREDITO', 'VISA', 1),
	(2, NULL, NULL, NULL, NULL, NULL, 'SQUA', 'SQUA', 1),
	(3, NULL, NULL, NULL, NULL, NULL, 'SCO', 'SCO', 1),
	(4, NULL, NULL, NULL, NULL, NULL, 'PERE', 'PERE', 1),
	(5, NULL, NULL, NULL, NULL, NULL, 'MASTERCARD', 'MC', 1),
	(6, NULL, NULL, NULL, NULL, NULL, 'VISA DEBITO', 'VD', 2);