Se ha solicitado el diseño de un sistema para procesar operaciones con tarjetas de crédito para una organización. Dicho sistema debe disponer de un módulo que permita con las siguientes consideraciones desarrollar un aplicativo:

·         Una tarjeta se identifica de acuerdo a la marca, número de tarjeta, cardholder y fecha de vencimiento

·         Una operación es válida en el sistema si la persona que opera en el mismo consume menos de 1000 pesos

·         Una tarjeta es válida para operar si su fecha de vencimiento es mayor al presente día

·         Hoy en día, existen tres marcas de tarjeta de crédito, a saber: “SQUA”, “SCO”, “PERE” y es posible que en los siguientes meses existan nuevas marcas. Cada marca tiene un modo de calcular una tasa por el servicio, a saber:

o   Tasa SQUA = año / mes

o   Tasa SCO    = dia del mes *0.5

o   Tasa PERE  = mes*0.1

 

Construir un desarrollo en java:

 

Parte 1

Crear una clase ejecutable con 3 objetos que haga lo siguiente:

a)                      Invocar un método que devuelva toda la información de una tarjeta

b)                      Informar si una operación es valida

c)                       Informar si una tarjeta es válida para operar

d)                      Identificar si una tarjeta es distinta a otra

e)                      Obtener por medio de un método la tasa de una operación informando marca e importe

 

Parte 2

Se identificó que el proceso que se ejecuta para cobrar consta de 4 acciones, a saber:

cobrar {

imprimirFactura():   //imprimir factura en controladora fiscal

enviarInfoTC();                      //enviar info de tarjeta de crédito

informarPago();                     //Informar pago a comercial

actualizarSaldo(cliente);        //actualizar saldo del cliente

}

Adaptar los métodos para contemplar los siguientes casos de error:

·               la impresora devuelve error (por ejemplo, por trabarse el papel)

·               el host de la tarjeta de crédito está caído

·               el sistema contable no responde y/o no atiende los pedidos

·               la base de datos no permite escribir el nuevo saldo del cliente

 

Parte 3

a)        Implementar un servicio REST en formato JSON de consulta de tasa de una operación informando marca e importe

b)        Compartir código en GIT

c)         Hostear solución API REST en algún cloud computing libre e indicar URL